# Partie 1
## Sous-partie 1 : texte
Une phrase sans rien

*Une phrase en italique*

**Une phrase ne gras**

Un lien vers [fun-mocc.fr](https://www.fun-mooc.fr/fr/)

Une ligne de `code`.

## Sous-partie 2 : listes
*Liste à puce*
- item
    * sous-item
    * sous-item
- item
- item

*Liste numérotée*
1. item
2. item
3. item

## Sous-partie 3 : code
```python
# Extrait de code